/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"io"
	"mime"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	logger "github.com/sirupsen/logrus"
	"gitlab.com/stageit-labs/nginx-ingress-default-backend/libs"
)

const (
	// FormatHeader name of the header used to extract the format
	FormatHeader = "X-Format"

	// CodeHeader name of the header used as source of the HTTP status code to return
	CodeHeader = "X-Code"

	// ContentType name of the header that defines the format of the reply
	ContentType = "Content-Type"

	// OriginalURI name of the header with the original URL from NGINX
	OriginalURI = "X-Original-URI"

	// Namespace name of the header that contains information about the Ingress namespace
	Namespace = "X-Namespace"

	// IngressName name of the header that contains the matched Ingress
	IngressName = "X-Ingress-Name"

	// ServiceName name of the header that contains the matched Service in the Ingress
	ServiceName = "X-Service-Name"

	// ServicePort name of the header that contains the matched Service port in the Ingress
	ServicePort = "X-Service-Port"

	// RequestID is a unique ID that identifies the request - same as for backend service
	RequestID = "X-Request-ID"

	// ErrFilesPathVar is the name of the environment variable indicating
	// the location on disk of files served by the handler.
	ErrFilesPathVar = "ERROR_FILES_PATH"
)

var (
	version string
	build   string
	date    string
)
var port string

func init() {
	if version == "" {
		version = "1.0.0"
	}
	if build == "" {
		build = "debug"
	}
	if date == "" {
		date = time.Now().UTC().String()
	}
	libs.InitLogger()
	libs.InitMetrics()
}

func main() {
	logger.Info("---------------------------------------------------")
	logger.Info("NGINX Default Backend")
	logger.Infof("Version: %s", version)
	logger.Infof("Build: %s", build)
	logger.Infof("Date: %s", date)
	logger.Info("---------------------------------------------------")

	port = os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	errFilesPath := "/www"
	if os.Getenv("LOCAL") != "" {
		errFilesPath = "./rootfs/www"
	}
	if os.Getenv(ErrFilesPathVar) != "" {
		errFilesPath = os.Getenv(ErrFilesPathVar)
	}

	http.HandleFunc("/", errorHandler(errFilesPath))

	http.Handle("/metrics", promhttp.Handler())

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	logger.Info("HTTP server started on port :" + port)
	http.ListenAndServe(":"+port, nil)
}

func errorHandler(path string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		ext := "html"

		if os.Getenv("DEBUG") != "" {
			w.Header().Set(FormatHeader, r.Header.Get(FormatHeader))
			w.Header().Set(CodeHeader, r.Header.Get(CodeHeader))
			w.Header().Set(ContentType, r.Header.Get(ContentType))
			w.Header().Set(OriginalURI, r.Header.Get(OriginalURI))
			w.Header().Set(Namespace, r.Header.Get(Namespace))
			w.Header().Set(IngressName, r.Header.Get(IngressName))
			w.Header().Set(ServiceName, r.Header.Get(ServiceName))
			w.Header().Set(ServicePort, r.Header.Get(ServicePort))
			w.Header().Set(RequestID, r.Header.Get(RequestID))
		}

		format := r.Header.Get(FormatHeader)
		if format == "" {
			format = "text/html"
			logger.Debugf("format not specified. Using %v", format)
		}

		cext, err := mime.ExtensionsByType(format)
		if err != nil {
			logger.Debugf("unexpected error reading media type extension: %v. Using %v", err, ext)
			format = "text/html"
		} else if len(cext) == 0 {
			logger.Debugf("couldn't get media type extension. Using %v", ext)
		} else {
			ext = cext[0]
		}
		w.Header().Set(ContentType, format)

		errCode := r.Header.Get(CodeHeader)
		code, err := strconv.Atoi(errCode)
		if err != nil {
			code = 404
			logger.Debugf("unexpected error reading return code: %v. Using %v", err, code)
		}
		w.WriteHeader(code)

		if !strings.HasPrefix(ext, ".") {
			ext = "." + ext
		}
		file := fmt.Sprintf("%v/%v%v", path, code, ext)
		f, err := os.Open(file)
		defer f.Close()
		if err != nil {
			logger.Debugf("unexpected error opening file: %v", err)
			scode := strconv.Itoa(code)
			file := fmt.Sprintf("%v/%cxx%v", path, scode[0], ext)
			f2, err := os.Open(file)
			defer f2.Close()
			if err != nil {
				logger.Debugf("unexpected error opening file: %v", err)
				http.NotFound(w, r)
				stop := time.Now()
				entry := logger.WithFields(logger.Fields{
					"remote_ip":     r.RemoteAddr,
					"protocol":      r.Proto,
					"host":          r.Host,
					"uri":           r.RequestURI,
					"method":        r.Method,
					"code":          strconv.Itoa(code),
					"referer":       r.Referer(),
					"user_agent":    r.UserAgent(),
					"latency":       strconv.FormatInt(stop.Sub(start).Nanoseconds(), 10),
					"latency_human": stop.Sub(start).String(),
				})
				entry.Info(r.Method + " " + r.URL.Path)
				return
			}

			logger.Debugf("serving custom error response for code %v and format %v from file %v", code, format, file)
			io.Copy(w, f2)
			stop := time.Now()
			entry := logger.WithFields(logger.Fields{
				"remote_ip":     r.RemoteAddr,
				"protocol":      r.Proto,
				"host":          r.Host,
				"uri":           r.RequestURI,
				"method":        r.Method,
				"code":          strconv.Itoa(code),
				"referer":       r.Referer(),
				"user_agent":    r.UserAgent(),
				"latency":       strconv.FormatInt(stop.Sub(start).Nanoseconds(), 10),
				"latency_human": stop.Sub(start).String(),
			})
			entry.Info(r.Method + " " + r.URL.Path)
			return
		}
		logger.Debugf("serving custom error response for code %v and format %v from file %v", code, format, file)
		io.Copy(w, f)

		stop := time.Now()
		entry := logger.WithFields(logger.Fields{
			"remote_ip":     r.RemoteAddr,
			"protocol":      r.Proto,
			"host":          r.Host,
			"uri":           r.RequestURI,
			"method":        r.Method,
			"code":          strconv.Itoa(code),
			"referer":       r.Referer(),
			"user_agent":    r.UserAgent(),
			"latency":       strconv.FormatInt(stop.Sub(start).Nanoseconds(), 10),
			"latency_human": stop.Sub(start).String(),
		})
		entry.Info(r.Method + " " + r.URL.Path)

		proto := strconv.Itoa(r.ProtoMajor)
		proto = fmt.Sprintf("%s.%s", proto, strconv.Itoa(r.ProtoMinor))
		libs.RequestCount.WithLabelValues(proto).Inc()
		libs.RequestDuration.WithLabelValues(proto).Observe(stop.Sub(start).Seconds())
	}
}
