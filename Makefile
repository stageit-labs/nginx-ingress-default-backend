.PHONY: build upload

build:
	docker build --squash -t registry.gitlab.com/stageit-labs/nginx-ingress-default-backend:v1.0.0  -t registry.gitlab.com/stageit-labs/nginx-ingress-default-backend:latest .
upload:
	docker push registry.gitlab.com/stageit-labs/nginx-ingress-default-backend:v1.0.0
	docker push registry.gitlab.com/stageit-labs/nginx-ingress-default-backend:latest